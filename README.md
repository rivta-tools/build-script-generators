# Generator för byggscript

Alla tjänstedomäner skall levereras med byggscript för att, baserat på tjänstekontrakten, generera grundläggande källkod i både Java och C#.

Detta repository innehåller Groovy-script som automatiskt genererar dessa byggscript.

##Ladda ner skripten

Aktuell version av scripten är taggen [release-1.3](https://bitbucket.org/rivta-tools/build-script-generators/src/release-1.3) i detta repository.

##Byggskript för Java (JAX-WS)

Ett skript som genererar en pom.xml. För att skriptet skall fungera krävs att:
 
 * Java är installerat
 * JAVA_HOME är satt
 * Groovy är installerat, minst version 1.8.1
 * GROOVY_HOME är satt och GROOVY_HOME/bin finns på path

Kör skriptet `JaxWsMavenPomGenerator.groovy` för att få mer info om parametrar för skriptet.


##Byggskript för .net (WCF)

Ett skript som genererar en bat-fil. För att skriptet skall fungera krävs att 

 * Java är installerat
 * JAVA_HOME är satt
 * Groovy är installerat, minst version 1.8.1
 * GROOVY_HOME är satt och GROOVY_HOME/bin finns på path
 * .NET framework är installerat.

Kör skriptet `WcfSvcutilBatfileGenerator.groovy` för att få mer info om parametrar för skriptet.